﻿using Heman.Calculator.Lib;
using System;
using System.Collections.Generic;
using MathNet;
using DynamicExpresso;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using Bogus;
using System.Dynamic;

namespace Heman.Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var target = new Interpreter();

            /*List<MaterialModel> materialModels = new List<MaterialModel>
            {
                new MaterialModel
                {
                    CS = 1,
                    EL = 2,
                    FR = 3,
                    IsLaser = false,
                    IsWelded = true,
                    Length = 1000.10,
                    Name = "MCH.0001",
                    Position = "X01.Y02",
                    SC = 4,
                    TH = 1,
                    ThicknesCode = "D",
                    Width = 1000.00
                }
            };*/

            Randomizer.Seed = new Random(8675309);

            var materialName = new[] { "MCH.0005", "MBH.0345", "MCF.4444", "MBB.4999", "MD1.0034" };
            var thiknes = new[] { "A", "B", "C", "D", "E" };

            var fake = new Faker<MaterialModel>()
                .RuleFor(p => p.Name, p=>p.PickRandom(materialName))
                .RuleFor(p => p.SC, p => p.Random.Number(1, 10))
                .RuleFor(p => p.CS, p => p.Random.Number(1, 10))
                .RuleFor(p => p.EL, p => p.Random.Number(1, 10))
                .RuleFor(p => p.FR, p => p.Random.Number(1, 10))
                .RuleFor(p => p.TH, p => p.Random.Number(1, 10))
                .RuleFor(p => p.IsLaser, p => p.Random.Bool())
                .RuleFor(p => p.IsWelded, p => p.Random.Bool())
                .RuleFor(p => p.ThicknesCode, p => p.PickRandom(thiknes))
                .RuleFor(p => p.Length, p => p.Random.Double(1000, 4000))
                .RuleFor(p => p.Width, p => p.Random.Double(1000, 4000))
                .RuleFor(p => p.Position, p => $"X{p.Random.Number(1,2000)}.Y{p.Random.Number(1, 2000)}")
                ;

            var testMaterials = fake.GenerateForever().Take(1000);

            var constants = GetConstants();

            var formulas = GetFormulas();

            var properties = typeof(MaterialModel).GetProperties();


            var watch = System.Diagnostics.Stopwatch.StartNew();
            

            List<List<Parameter>> results = new List<List<Parameter>>();
            foreach (var item in testMaterials)
            {
                List<Parameter> parameters = new List<Parameter>();
                foreach (var property in properties)
                {
                    parameters.Add(new Parameter(property.Name, property.GetValue(item, null)));
                }
                foreach (var formula in formulas.Formulas)
                {
                    var myFunc = target.Parse(formula.Value, parameters.ToArray());
                    var formulaResult = myFunc.Invoke(parameters);
                    parameters.Add(new Parameter(formula.Key, formulaResult));
                }
                results.Add(parameters);
            }

            watch.Stop();
            var elapsedMs = watch.Elapsed;
            Console.WriteLine(elapsedMs);

            dynamic result = results.Select(d=> d.Aggregate(new ExpandoObject() as IDictionary<string, Object>,
                            (a, p) => { a.Add(p.Name, p.Value); return a; })).ToList();

            //var x = myFunc.Compile<int>();
            // wyswietlenie tabeli z wynikami 
            // generowanie duzej liczby danych
            // zamiast klasy uzyc slownika, co pozwoli po wyliczeniu jednej z formul dodania jej do parametrow dzieki,
            // czemu bedzie mozliwosc dynamicznego dodawania formul do kolejnych formol 
            // np. sum12 = ThicknesCode == \"D\" ? SC : (TH + FR) * EL
            // sum1 = sum12 * 34
            // zrobic formuly i consty statyczne przez co beda szybsze 



            Console.ReadLine();
        }

        private static FormulasModel GetFormulas()
        {
            FormulasModel formulas = new FormulasModel();
            using (StreamReader r = new StreamReader("formulas.json"))
            {
                string json = r.ReadToEnd();
                formulas = JsonConvert.DeserializeObject<FormulasModel>(json);
            }
            return formulas;
        }

        private static Constants GetConstants()
        {
            Constants constants = new Constants();

            using (StreamReader r = new StreamReader("constants.json"))
            {
                string json = r.ReadToEnd();
                constants = JsonConvert.DeserializeObject<Constants>(json);
            }
            return constants;
        }
    }
}
