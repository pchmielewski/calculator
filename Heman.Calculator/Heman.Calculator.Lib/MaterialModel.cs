﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Heman.Calculator.Lib
{
    public class MaterialModel
    {
        public string Name { get; set; }
        public string Position { get; set; }
        public bool IsWelded { get; set; }
        public bool IsLaser { get; set; }
        public string ThicknesCode { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
        public int TH { get; set; }
        public int SC { get; set; }
        public int FR { get; set; }
        public int EL { get; set; }
        public int CS { get; set; }
    }
}
