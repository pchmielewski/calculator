﻿using System.Collections.Generic;

namespace Heman.Calculator.Lib
{
    public class Factory
    {
        public string Name { get; set; }
        public Dictionary<string, string> Constants { get; set; }
    }
}